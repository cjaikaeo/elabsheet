from django.template import Library
from django.urls import get_script_prefix
from django.conf import settings

register = Library()

@register.simple_tag
def script_prefix():
    """
    Returns script prefix for url.  It basically returns "django.root".
    """
    return get_script_prefix()


@register.simple_tag
def site_name():
    """
    Returns the site name specified in the settings file
    """
    return settings.SITE_NAME


@register.simple_tag
def elab_version():
    """
    Returns the version as specified in the settings file
    """
    return settings.VERSION


@register.simple_tag
def use_kusso():
    """
    Returns True if KUSSO config is found
    """
    return hasattr(settings, 'KUSSO_CONFIG')
