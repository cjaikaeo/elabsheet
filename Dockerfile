FROM ubuntu:18.04

ARG DEBIAN_FRONTEND=noninteractive

WORKDIR /

RUN apt-get update \
 && dpkg --add-architecture i386 \
 && apt-get install -y \
      locales wget \
      build-essential g++-multilib \
      python-minimal python-dev \
      openjdk-17-jdk-headless \
      mono-mcs \
      mysql-client libmysqlclient-dev

RUN locale-gen en_US.UTF-8 \
 && ln -s /usr/bin/mcs /usr/bin/gmcs

RUN adduser --disabled-password --gecos "" elab \
 && adduser --disabled-password --gecos "" elabdummy \
 && adduser elabdummy elab

RUN apt-get update \
 && apt-get install -y git python3 python3-dev python3-venv python3-pip

RUN wget -q https://github.com/mathjax/MathJax/archive/2.7.9.tar.gz \
 && tar xf 2.7.9.tar.gz \
 && mv MathJax-2.7.9 mathjax \
 && rm 2.7.9.tar.gz

COPY ./requirements.txt /

RUN pip3 install --upgrade pip \
 && pip3 install -r /requirements.txt \
 && pip3 install --no-cache-dir gunicorn 'numpy<1.20'

RUN ln -s /home/elab/app/lib/matplotlib-elab /usr/local/lib/python3.6/dist-packages/matplotlib

RUN cd /opt \
 && wget -q https://elab.cpe.ku.ac.th/download/elab/python-3.12.6.bin.ubuntu-1804.tgz \
 && tar xf python-3.12.6.bin.ubuntu-1804.tgz \
 && rm python-3.12.6.bin.ubuntu-1804.tgz \
 && ln -s /opt/python-3.12.6/bin/python3.12 /usr/local/bin/python3.12

RUN /opt/python-3.12.6/bin/pip3 install --no-cache-dir numpy

RUN ln -s /home/elab/app/lib/matplotlib-elab /opt/python-3.12.6/lib/python3.12/matplotlib

COPY --chown=elab:elab . /home/elab/app

USER elab
RUN (cd /home/elab/app; ./install-dirs.sh)

USER root
RUN (cd /home/elab/app; ./install-box.sh)

WORKDIR /home/elab/app/
